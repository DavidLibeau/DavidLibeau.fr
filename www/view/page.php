<?php 
if(isset($_GET["page"]) && $_GET["page"]!=""){
    $page = ucfirst(strtolower($_GET["page"]));

$title="Page";
require("../page/".$page."-info.php");
?>
<!DOCTYPE html>

<html lang="<?php if(isset($lang)){ echo($lang); }else{ echo("fr"); } ?>">

<head>
    <meta charset="utf-8" />
    <title>DavidLibeau.fr - <?php echo($title); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#0070C0">
    <link rel="icon" href="/img/favicon.png" type="image/png" />
    <link rel="stylesheet" href="/style/page.css" />
    <link href="https://dav.li/fontawesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
</head>

<body>
<?php if(!isset($_GET["iframe"])){ ?>
   <nav id="homeBtn"><a href="/"><i class="fa fa-arrow-left" aria-hidden="true"></i> DavidLibeau.fr</a></nav>
<?php } ?>
    <main>
       <h1><?php echo($title); ?></h1>
        <?php require("../page/".$page."-body.php"); ?>
    </main>


    <script src="https://dav.li/jquery/3.1.1.min.js"></script>
    <script src="script/script.js"></script>
</body>

</html>


<?php }else{
    echo("404 Error");
} ?>