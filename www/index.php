<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>DavidLibeau.fr</title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#0070C0">
    <link rel="manifest" href="/manifest.json">
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link rel="stylesheet" href="style/style.css" />
    <link href="https://dav.li/fontawesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
</head>

<body>
    <main>
        <section id="logo">
            <h1><img src="img/LogoAnimeDavidLibeau.gif" alt="David Libeau Logo" draggable="false" /></h1>
        </section>
        <section id="content">
            <ul>
                <li>
                    <a href="https://blog.davidlibeau.fr" target="_blank">
                        <h2>Blog<i class="fa fa-external-link" aria-hidden="true"></i></h2>
                        <p>Mon blog personnel où vous pouvez trouver bon nombre d'articles</p>
                    </a>
                </li>
                <!--li>
                    <a href="" target="_blank">
                        <h2>Freelance<i class="fa fa-external-link" aria-hidden="true"></i></h2>
                        <p>Si vous souhaitez faire appel à mes services</p>
                    </a>
                </li-->
                <li>
                    <a href="https://lab.dav.li" target="_blank">
                        <h2>Lab<i class="fa fa-external-link" aria-hidden="true"></i></h2>
                        <p>Répertoire de mes projets open source</p>
                    </a>
                </li>
                <li>
                    <a href="CV">
                        <h2>CV</h2>
                        <p>Mon Curriculum Vitae</p>
                    </a>
                </li>
                <li>
                    <a href="Contact">
                        <h2>Contact</h2>
                        <p>Si vous souhaitez m'envoyer un message</p>
                    </a>
                </li>
                <li>
                    <a href="About">
                        <h2>À propos</h2>
                        <p>Plus d’informations sur ce site web</p>
                    </a>
                </li>
            </ul>
        </section>
    </main>


    <script src="https://dav.li/jquery/3.1.1.min.js"></script>
    <script src="script/script.js"></script>
</body>

</html>