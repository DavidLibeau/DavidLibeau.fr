<style>
    h1 {
        display: none;
    }

    header {
        background-color: #f0f0f0;
        color: #0070c0;
        padding: 20px;
        border-radius: 20px;
        font-size: 2em;
    }
    
    h2 {
        font-family: "Bebas Neue", sans-serif;
        text-align: center;
        font-size: 2em;
    }

    .experience__desc {
        font-style: italic;
    }
    
    article {
        display: block;
        position: relative;
        padding-left: 30px;
        padding-bottom: 10px;
        margin-top: -20px;
    }
    article::before{
        content: '';
        position: absolute;
        top: 0;
        left: 10px;
        bottom: -7px;
        width: 2px;
        transform: translateX(-50%);
        background: #424242;
    }
    article:last-child::before{
        bottom: 20px;
    }
    article::after{
        content: '';
        position: absolute;
        top: 0;
        left: 2px;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        border: 3px solid #fff;
        background: #424242;
    }
</style>
<header>
    <h2>David Libeau</h2>
</header>
<section>
    <h2 class="section__title">Work experience</h2>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">Research engineer</h3>
        <h4 class="experience__subtitle">Arcom</h4>
        <div class="experience__metas">11/2023 – Current (Paris, France)</div>
        <div class="experience__desc">
            <p>Research engineer working on the DSA and other related regulations applicable to online platforms.</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">Full-stack developer</h3>
        <h4 class="experience__subtitle">Cour de cassation</h4>
        <div class="experience__metas">12/2021 – 11/2023 (Paris, France)</div>
        <div class="experience__desc">
            <p>Open source developer of a text annotation software that anonymise court decision in order to publish
                them in open data which has an high GDPR &amp; privacy impact.</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">Backend developer</h3>
        <h4 class="experience__subtitle">Leboncoin (Adevinta)</h4>
        <div class="experience__metas">01/2020 – 11/2020 (Paris, France)</div>
        <div class="experience__desc">
            <p>Backend developer of a data importer software.</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">Web developer</h3>
        <h4 class="experience__subtitle">Université de Rennes 1</h4>
        <div class="experience__metas">09/2016 – 08/2017 (Rennes, France)</div>
        <div class="experience__desc">
            <p>Apprenticeship as web developer at the communication department of the University of Rennes 1.</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">Video maker &amp; webmaster</h3>
        <h4 class="experience__subtitle">Onisep Bretagne</h4>
        <div class="experience__metas">09/2015 – 08/2016 (Rennes, France)</div>
        <div class="experience__desc">
            <p>Apprenticeship as video maker &amp; webmaster for the Britany agency of the french student advisor
                agency.</p>
        </div>
    </article>
</section>
<section>
    <h2 class="section__title">Education and training</h2>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">University of York</h3>
        <h4 class="experience__subtitle">PhD student for one year</h4>
        <div class="experience__metas">11/2020 – 12/2021 (York, United Kingdom) </div>
        <ul>
            <li>Field(s) of study: <strong>Inter-disciplinary
                    programmes and qualifications involving Information and Communication Technologies (ICTs)
                </strong></li>
            <li class="inline-list__item">
                Final grade: <strong>Not finished due to funding issues</strong>
            </li>
        </ul>
        <div class="experience__desc">
            <p>Accessibility of gaming live streaming thanks to auto-generated audio description for visually
                impaired gamers with the use of game's data (https://gametospeech.com)</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">University of Skövde</h3>
        <h4 class="experience__subtitle">MSc</h4>
        <div class="experience__metas">09/2018 – 08/2019 (Skövde, Sweden)</div>
        <ul>
            <li>Field(s) of study: <strong>Inter-disciplinary
                    programmes and qualifications involving Information and Communication Technologies (ICTs)
                </strong></li>
        </ul>
        <ul>
            <li>
                Thesis: Audience participation games
            </li>
        </ul>
        <div class="experience__desc">
            <p>Scientific research and design in the gaming field.</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">Université de Montréal</h3>
        <h4 class="experience__subtitle">DESS</h4>
        <div class="experience__metas">09/2017 – 08/2018 (Montreal, Canada) </div>
        <ul>
            <li>Field(s) of study: <strong>Audio-visual techniques
                    and media production</strong></li>
        </ul>
        <div class="experience__desc">
            <p>Art, creation and technologies.</p>
        </div>
    </article>
    <article class="experience experience--timeline">
        <h3 class="experience__title experience__title--color">IUT de Laval</h3>
        <h4 class="experience__subtitle">BSc</h4>
        <div class="experience__metas">09/2014 – 08/2017 (Laval, France)</div>
        <ul>
            <li>Field(s) of study: <strong>Inter-disciplinary
                    programmes and qualifications involving Information and Communication Technologies (ICTs)
                </strong></li>
        </ul>
        <div class="experience__desc">
            <p>Multimedia, gaming and web technologies.</p>
        </div>
    </article>
</section>