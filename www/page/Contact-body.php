<p>Vous pouvez me contacter par différents moyens :</p>
<h3>Via un formulaire de contact</h3>
<ul class="extLinks">
    <li><a href="/Bonjour">Formulaire de contact en français</a></li>
    <li><a href="/Hello">Contact form in english</a></li>
</ul>
<h3>Via un réseau social</h3>
<ul class="extLinks">
    <li><a rel="me" href="https://mastodon.xyz/@David" target="_blank">Mastodon @David@Mastodon.xyz</a></li>
    <li><a rel="me" href="https://twitter.com/DavidLibeau" target="_blank">Twitter @DavidLibeau</a></li>
    <li><a rel="me" href="https://www.linkedin.com/in/davidlibeau/" target="_blank">Linkedin @DavidLibeau</a></li>
</ul>
<h3>Je suis aussi sur</h3>
<ul class="extLinks">
    <li><a rel="me" href="https://framagit.org/DavidLibeau" target="_blank">GitLab (Framagit)</a></li>
    <li><a rel="me" href="https://github.com/DavidLibeau" target="_blank">GitHub</a> [plus de nouveaux projets sur GitHub depuis le rachat par Microsoft]</li>
    <li><a rel="me" href="https://davidlibeau.itch.io/" target="_blank">Itch.io</a></li>
    <li><a rel="me" href="https://www.instructables.com/member/DavidLibeau/" target="_blank">Instructables</a></li>
</ul>