<p><strong>DavidLibeau.fr</strong>, <em>le site web de David Libeau (comme son nom l'indique)</em>.</p>
<h3>Qui est David Libeau ?</h3>
<p>Un gars cool, jetez un oeil à <a href="CV" target="_blank">mon CV</a> !</p>
<h3>Qui a fait ce site web ?</h3>
<p>Comme je suis développeur web, je l'ai fait moi-même ! Plutôt logique.</p>
<h3>Comment l'as-tu fait ?</h3>
<p>Vous pouvez remonter à la source du site web <a href="https://framagit.org/DavidLibeau/DavidLibeau.fr" target="_blank">sur GitLab (Framagit)</a>, et ainsi tout voir et tout comprendre !</p>
<h3>Y a-t-il autre chose à savoir ?</h3>
<p>Oui, vous pouvez maintenant retrouver une déclaration d'intérêt et de ressources sur <a href="transparence">la page Transparence</a> !</p>