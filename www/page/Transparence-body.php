<p>Retrouvez sur cette page mes déclarations d'intérêt, les rémunérations perçues et les rémunérations versées pour des services non-domestiques.</p>
<p>Les montants sont arrondis au milier et seules les rémunérations supérieures à 1000€ sont déclarées. Les rémunérations familliales ne sont pas déclarées. Les mandats bénévoles sont déclarés.</p>
<h3>Déclaration d'intérêt</h3>
<p>A ce jour, je ne déclare aucun intérêt.</p>
<h3>Déclaration de revenus</h3>
<ul>
<?php
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen('../data/transparence.csv', "r");
for ($i = 0; $row = fgetcsv($handle ); ++$i) {
    for ($j = 0; $line = fgetcsv($handle); ++$j) { 
        echo("<li>$line[0] ($line[1] mois) : $line[2]$line[3]<br/><em> $line[4] — $line[5]</em></li>");
    }
}
fclose($handle);
ini_set('auto_detect_line_endings',FALSE);
?>
</ul>
<p>Ces données sont disponibles au format CSV <a href="/data/transparence.csv">à cette adresse</a>.</p>