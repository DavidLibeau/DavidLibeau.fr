<div id="search-form">
<?php get_search_form(); ?>
</div>
<?php if (have_posts()) : ?>
<ul class="post">
  <?php while (have_posts()) : the_post(); ?>
    <li id="<?php the_ID(); ?>" <?php post_class(); ?>>
    <span class="circle"></span>
      <h3 class="post-title">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <?php if(has_tag("en")){ ?><span class="lang-tag" title="in english">en</span><?php } ?>
      </h3>
      <div class="post-excerpt">
        <?php the_excerpt(); ?>
      </div>
      <p class="post-info">
        – le <?php the_date(); ?> dans <?php the_category(', '); ?> par <?php the_author(); ?>.
      </p>
    </li>
  <?php endwhile; ?>
</ul>
<?php else : ?>
  <p class="nothing">
    Il n'y a pas de Post à afficher !
  </p>
<?php endif; ?>
