<!DOCTYPE html>
<html>
  <head <?php if(has_tag("en")){ ?>lang="en-EN"<?php }else{ language_attributes(); } ?> >
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#0070C0">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <div class="content">
      <header>
        <?php if (is_single()) : ?>
          <h1><a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a></h1>
        <?php else : ?>
          <h1><a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a></h1>
        <?php endif; ?>
      </header>