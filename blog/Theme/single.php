<?php get_header(); ?>
<div class="main">
  <?php if (have_posts()) : ?>
    <ul class="post">
    <?php while (have_posts()) : the_post(); ?>
      <li class="post">
        <span class="circle"></span>
        <p class="post-tags"><?php the_tags("", " "); ?></p>
        <h1 class="post-title"><?php the_title(); ?></h1>
        <p class="post-info post-info-mini">
        – le <?php the_date(); ?> par <?php the_author(); ?>
      	</p>
        <div class="post-content">
          <?php the_content(); ?>
        </div>
    	<p class="post-info">
        – le <?php echo(get_the_date()); ?> (modifié le <?php the_modified_date(); ?>) dans <?php the_category(', '); ?> par <?php the_author(); ?>.
      	</p>
      </li>
        <div class="post-author">
            <h3>A propos de l'auteur</h3>
            <p><a href="https://davidlibeau.fr" target="_blank">David Libeau</a> est développeur, concepteur et chercheur dans les domaines des données ouvertes, des jeux vidéos, de l'accessibilité et du web. Son doctorat vise à rendre accessibles les streams de jeux vidéo en générant automatiquement de l'audio description à partir des données des jeux vidéo (c.f. <a href="https://gametospeech.com"  target="_blank">gametospeech.com</a>).</p>
        </div>
        <hr style="width: 25%; margin: auto;" />
        <div class="post-comments">
          <?php comments_template(); ?>
        </div>
    <?php endwhile; ?>
  <?php endif; ?>
</div>
<?php get_footer(); ?>